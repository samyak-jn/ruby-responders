source 'https://rubygems.org'

gemspec path: '..'

gem 'activemodel', github: 'rails/rails', branch: '6-0-stable'
gem 'railties', github: 'rails/rails', branch: '6-0-stable'
gem 'mocha'
gem 'rails-controller-testing'
